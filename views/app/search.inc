<legend>Search news</legend>
<div class="container bs-component">
<form method='POST' enctype='multipart/form-data' class="form-inline" role="form">
	<input type="text" class="form-control input-large" name='text' placeholder="Enter text to search">
	<select class="form-control" id="select" name="category">
		<option value="" disabled selected>Choose category</option>
		<?foreach (AppController::$categories as $category) {
			echo "<option value='$category'>$category</option>";
		}?>
	</select>
	<button type="submit" class="btn btn-default">Search</button>
</form>
</div>
<br>

	<? if(!empty($parameters)){?>
		<legend>Results</legend>
	<?} ?>
	 	<?
		$articles = $parameters;
		foreach ($articles as $article) {
	?>	
		<div class="container well bs-component">
		  <h4 class="itemtitle">
			<a href="<?=$self.'/showNews/?id='.$article['id']?>">
				<?=$article['title']?>
			</a>
		  </h4>
		  <h7 class="itemposttime white-text">
			<span>
			  Posted:
			</span>
		  <?=date("H:i d.m.Y",$article['pubDate'])?>
		   by <?=$article['author']?>
		  </h7>
		  <h6>
			  <div class="containter white-text" name="decodeable">
				<?=$article['description']?>
			  </div>
		  </h6>
		  
		</div>
		<br>
	<?
		}
		?>
	