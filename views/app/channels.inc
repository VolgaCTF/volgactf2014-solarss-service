
<? $categories = $parameters;
   if(empty($categories)){
   		?>
   		<div>
			<div class="alert alert-warning" role="alert">No channels where found =\</div>
			<br>
			<a href='javascript:history.back()'> Go Back </a>
		</div>
   		<?
   }
   else{
   		foreach ($categories as $category => $channels) {
   			?>
   				<legend><?=$category?></legend>
				<div class="list-group">
					<?
					foreach($channels as $channel) {
					?>
						<a href="<?=$self.'/channel/?id='.$channel['id']?>" class="list-group-item">
							<span class="badge"><?=$channel['count']?></span>
							<?=$channel['name']?>
						</a>
					<?
					}
					?>
				</div>	
				</ul>

   			<?
   		}
	}
?>