<legend>Add an article</legend>
<div class="well bs-component">
	<form class="form-horizontal" method="POST">
	<fieldset>
		<div class="form-group">
			<label for="inputTitle" class="col-lg-2 control-label">Title</label>
			<div class="col-lg-10">
				<input type="text" class="form-control" id="inputTitle" name="title" placeholder="Title">
			</div>
		</div>
		<div class="form-group">
			<label for="select" class="col-lg-2 control-label">Channel</label>
			<div class="col-lg-4">
				<select class="form-control" id="select" name="channelId">
					<option value="" disabled selected>Choose channel</option>
					<?
						$channels = $parameters;
						foreach ($channels as $channel) {
							echo "<option value='{$channel['id']}'>{$channel['name']}</option>";
						}
					?>
				</select>
			</div>
		</div>
		<div class="form-group">
			<label for="inputDescription" class="col-lg-2 control-label">Description</label>
			<div class="col-lg-10">
				<input type="text" class="form-control" id="inputDescription" name="description" placeholder="Description">
			</div>
		</div>
		<div class="form-group">
			<label for="textArea" class="col-lg-2 control-label">Textarea</label>
			<div class="col-lg-10">
				<textarea class="form-control" rows="3" id="textArea" name="content"></textarea>
				<span class="help-block">You can use any html you want, but it's recomended not to change style.</span>
			</div>
		</div>
		<div class="form-group">
			<div class="col-lg-10 col-lg-offset-2">
				<button type="submit" class="btn btn-primary">Submit</button>
			</div>
		</div>
	</fieldset>
</form>
</div>