<div class="well bs-component">
	<?
	$article = $parameters[0];
?>	
<div class="post shortcuts_item focus" id="post_230339">
  	<div class="published"> <?=date("H:i d.m.Y",$article['pubDate'])?></div>
	<h2 class="title">
		  <a href="<?=$article['link']?>" class="post_title"><?=$article['title']?></a>
  	</h2>
  	
  	<div>
		<a href="<?=$self.'/category/?category='.$article['category']?>"><?=$article['category']?></a><span>*</span>
	</div>

  	
  	<div class="container  bs-component">
		<?=$article['description']?>
	</div>
	<br>
	<div class="bs-component">
		<h6>
		<?=$article['content']?>
		</h6>
	</div>
	<div class="page-header"></div>
</div>