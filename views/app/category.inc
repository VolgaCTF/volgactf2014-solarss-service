<legend>Categories</legend>

<?
	$articles = $parameters;
	foreach ($articles as $article) {
?>	
	<div class="container well bs-component">
		  <h4>
			<a href="<?=$self.'/showNews/?id='.$article['id']?>">
				<?=$article['title']?>
			</a>
		  </h4>
		  <h7 class="itemposttime white-text">
			<span>
			  Posted:
			</span>
		  <?=date("H:i d.m.Y",$article['pubDate'])?> by <?=$article['author']?>
		  </h7>
		  <h6>
			  <div class="white-text">
				<?=$article['description']?>
			  </div>
		  </h6>
		  
		</li>
	</div>
	<br/>
<?
	}
	?>
