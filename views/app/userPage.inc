
<div class="row">
	<div class="col-lg-8">
		<div class="bs-component">
			<legend>Your channels</legend>
			<div class="list-group">
				<?
				$channels = $parameters['channels'];
				foreach($channels as $channel) {
				?>
					<a href="<?=$self.'/channel/?id='.$channel['id']?>" class="list-group-item">
							<span class="badge"><?=$channel['count']?></span>
							<?=$channel['name']?>
					</a>
				<?
				}
				?>
			</div>
		</div>

		<div class="bs-component">
			<legend>Your subscribes</legend>
				<?
				$feeds = $parameters['feeds'];
				foreach($feeds as $feed) {
				?>
					<h5><?=$feed['link']?></h5>
					
						<?
						$articles = $feed['articles'];
						foreach ($articles as $article) {
						?>	
							<div class="container well col-lg-12">
							  <h4 class="itemtitle">
								<a href="<?=$article['link']?>">
									<?=$article['title']?>
								</a>
							  </h4>
							  <h7 class="itemposttime white-text">
								<span>
								  Posted:
								</span>
							  <?=date("H:i d.m.Y",$article['pubDate'])?>
							  </h7>
							  <h6>
								  <div class="itemcontent white-text" name="decodeable">
									<?=$article['description']?>
								  </div>
							  </h6>
							  
							</div>
							<div class="page-header"></div>
						<?
						}
						?>
					
					<?
				}
				?>
			</ul>
		</div>








	</div>
	

	<div class="col-lg-4">
		<div class="well bs-component">
			<form class="form-horizontal" method="POST">
				<input type='hidden' name='action' value='createChannel'>
				<fieldset>
					<legend>Channels</legend>
					<h6>Create channel</h6>
					<div class="form-group">
						<label for="channelName" class="col-lg-2 control-label">Name</label>
						<div class="col-lg-10">
							<input type="text" class="form-control" id="channelName" placeholder="Name" name="channelName">
						</div>
					</div>
					<div class="form-group">
						<label for="select" class="col-lg-2 control-label">Category</label>
						<div class="col-lg-10">
							<select  class="form-control" id="select"  name="category">
								<option value="" disabled selected>Choose category</option>
								<?foreach (AppController::$categories as $category) {
									echo "<option>$category</option>";
								}
								echo "<option>Private</option>";
								?>
							</select>
						</div>
					</div>
					<div class="form-group">
						<div class="col-lg-10 col-lg-offset-2">
							<button type="submit" class="btn btn-primary">Submit</button>
						</div>
					</div>
				</fieldset>
			</form>
			<?
				$channels = $parameters['channels'];
				foreach ($channels as $channel) {
					?>
					<form method='POST' id='del_channel_<?=$channel['id']?>'>
						<input type='hidden' name='action' value='deleteChannel'>
						<input type='hidden' name='channelId' value='<?=$channel['id']?>'>
						<li class="list-group-item">
							<span class="badge pointer" onclick="del_channel_<?=$channel['id']?>.submit()">X</span>
							<?=$channel['name']?>
						</li>
					</form>

					<?
				}
			?>

		</div>

		<div class="well bs-component">
			<form class="form-horizontal" method="POST">
				<input type='hidden' name='action' value='subscribeFeed'>
				<fieldset>
					<legend>Feeds</legend>
					<h6>Subscribe feed</h6>
					<div class="form-group">
						<label for="feedURI" class="col-lg-2 control-label">URI</label>
						<div class="col-lg-10">
							<input type="text" class="form-control" id="feedURI" placeholder="URI" name="feedURI">
						</div>
					</div>
					<div class="form-group">
						<div class="col-lg-10 col-lg-offset-2">
							<button type="submit" class="btn btn-primary">Subscribe</button>
						</div>
					</div>
				</fieldset>
			</form>
			<?
				$feeds = $parameters['feeds'];
				foreach ($feeds as $feed) {
					?>
					<form method='POST' id='del_feed_<?=$feed['id']?>' >
						<input type='hidden' name='action' value='deleteFeed'>
						<input type='hidden' name='feedId' value='<?=$feed['id']?>'>
						<li class="list-group-item">
							<span class="badge pointer" onclick="del_feed_<?=$feed['id']?>.submit()">X</span>
							<?=substr($feed['link'],0,35).'...'?>
						</li>
					</form>

					<?
				}
			?>
		</div>

	</div>
</div>
