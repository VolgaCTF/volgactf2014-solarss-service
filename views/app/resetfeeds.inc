<legend>Reset your feeds</legend>
<div class="alert alert-dismissable alert-warning">
  <button type="button" class="close" data-dismiss="alert" onclick='location.href="<?=$self?>/"'>×</button>
  <h4>Warning!</h4>
  <p>
  	This action will remove all your subscribed feeds, there'll be no way to recover this information.<br>Are you sure?
  </p>
</div>
  <form method="POST">
  	<input type='hidden' name='sure' value='ok'>
  	<input type='submit' class="btn btn-primary" value='Delete feeds'>
    <input onclick='location.href="<?=$self?>/"' class="btn btn-default" value='Back to main page'></button>
  </form>