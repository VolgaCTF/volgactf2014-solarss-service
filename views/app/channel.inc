<legend><?=$parameters['name']?> by <?=$parameters['author']?></legend>
<a href="<?=$_SERVER['REQUEST_URI']?>&format=XML">RSS FEED</a>
<?
	$articles = $parameters['articles'];
	foreach ($articles as $article) {
?>	
	<div class="container well bs-component">
			<h4>
				<a href="<?=$self.'/showNews/?id='.$article['id']?>">
					<?=$article['title']?>
			</a>
			</h4>
			<h7 class="itemposttime white-text">
				<span>
					Posted:
				</span>
			<?=date("H:i d.m.Y",$article['pubDate'])?>
			</h7>
			<h6>
				<div class="white-text">
					<?=$article['description']?>
				</div>
			</h6>
		</li>
	</div>
	<br/>
<?
	}
	?>
