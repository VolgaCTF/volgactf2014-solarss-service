<legend>Register Page</legend>
<div class="well bs-component">
	<form method='POST' enctype='multipart/form-data' action='<?=$self?>/register/' class="form-horizontal" role="form">
		<div class="form-group">
			<label for="login" class="col-lg-2 control-label">Login</label>
		 	<div class="col-sm-9">
		 		<input type='text' name='login' placeholder='login' class="form-control" size='32'><br>
			</div>
		</div>
		<div class="form-group">
			<label for="password" class="col-sm-2 control-label white-text">Password</label>
		 	<div class="col-sm-9">
		 		<input type='password' name='password' placeholder='password' class="form-control" size='32'><br>
			</div>
		</div>
			
		<div class="form-group">
		  <div class="col-sm-offset-2 col-sm-9">
			 <button type="submit" class="btn btn-primary">Submit</button>
		  </div>
	   </div>
		<div class="form-group">
		  <div class="col-sm-offset-2 col-sm-9">
			<a href='<?=$self?>/login/'>Login</a>
		  </div>
	   </div>
   	</form>
</div>


