
  <div id="discovery-top" data-tracking-area="discovery-north" style="position: static; visibility: visible; display: block; width: 100%;">
	<div id="discovery-main-c492" class="discovery-main discovery-thumbnails style-variant-big-final doublethumbnails">
	  <section id="col-promoted-c492" class="col-promoted discovery-col-0 ">
		<ul class="discovery-posts">

			<?
			$articles = $parameters;
				foreach ($articles as $article) {
?>
		  <li class="discovery-post hasthumbnail post-3" id="discovery-link-promoted-133937" data-role="discovery-post">
			<a href="<?=$self.'/'.$article['link']?>" rel="nofollow norewrite" class="publisher-anchor-color">
			  <div class="thumbnail" style="background-image: url(<?=$self.'/layout/images/'.$article['category'].'.jpg'?>); background-position:center; ">
			  </div>
			  <header class="discovery-post-header">
				<h3 title="10 Most Beautiful and Colorful Cities in the World">
				  <span data-role="discovery-thread-title" class="title line-truncate" data-line-truncate="4">
					<?=$article['title']?>
				  </span>
				  <span class="inline-meta" orrnclick='location.href="<?=$self.'/category/?category='.$article['category']?>"'>
					<?=$article['category']?>
				  </span>
				</h3>
			  </header>
			</a>
		  </li>
<?				}
			?>







		</ul>
	  </section>
	</div>
  </div>
</div>