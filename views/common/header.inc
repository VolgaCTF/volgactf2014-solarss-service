<? 
  	$a = explode('/', $_SERVER['PHP_SELF']);
  	array_pop($a);
  	$self =  implode('/', $a);
?>

<!DOCTYPE html>
<html lang="en">

  <head>
	<meta charset="utf-8">
	<link rel="icon" href="favicon.ico">
	<title>SolaRSS</title>

	<link href="!<?=$self?>/layout/css/docs.min.css" rel="stylesheet">
	<link href="!<?=$self?>/layout/css/normalize.css" rel="stylesheet">
	<link href="!<?=$self?>/layout/css/bootstrap-theme.min.css" rel="stylesheet">
	
	<!-- Bootstrap core CSS -->
	<link href="http://bootswatch.com/cyborg/bootstrap.min.css" rel="stylesheet">
	<!-- Optional Bootstrap Theme -->
	 <link href="data:text/css;charset=utf-8," data-href="../dist/css/bootstrap-theme.min.css" rel="stylesheet" id="bs-theme-stylesheet">
	<!-- Documentation extras -->
	<link href="!<?=$self?>/layout/css/docs.min.css" rel="stylesheet">
	<link href="!<?=$self?>/layout/css/lounge.css" rel="stylesheet">
	<link href="<?=$self?>/layout/css/discovery.css" rel="stylesheet">
	

	<style>
		.lcs {color: red;}
		.centered{float: none;margin: 0 auto;}
		.content{max-width:300px; word-wrap:break-word;}
		.td_name{max-width:150px; word-wrap:break-word;}
	  body {background-image:url('<?=$self?>/layout/images/Infant_Stars_in_Orion.jpg'); background-repeat: no-repeat;background-attachment: fixed;}
	  .input-large{width:70% !important;}
	  img{max-height:100% !important; max-width:100% !important;}
	  .pointer{cursor:pointer !important;}
	  zdiv{word-wrap:break-word;}
	</style>
  </head>
  <body role="document">


<header class="navbar navbar-default navbar-fixed-top" id="top" role="banner">
  <div class="container">
	<div class="navbar-header">
	  <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".bs-navbar-collapse">
		<span class="sr-only">Toggle navigation</span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
	  </button>
	  <a href="<?=$self?>/" class="navbar-brand">Solar RSS</a>
	</div>
	<nav class="collapse navbar-collapse bs-navbar-collapse" role="navigation">
	  <ul class="nav navbar-nav">
		<li>
		  <a href="<?=$self?>/search/">Search</a>
		</li>
		<li>
		  <a href="<?=$self?>/channels/">Public channels</a>
		</li>
		<li>
		  <a href="<?=$self?>/add/">Add news</a>
		</li>
		<li>
		  <a href="<?=$self?>/userPage/">My Page</a>
		</li>
	  </ul>
	  <ul class="nav navbar-nav navbar-right">
		<li>
		  <a href="<?=$self?>/resetFeeds/">Reset feeds</a>
		</li>
		<li>
		  <a href="<?=$self?>/<?=(UserController::isAuth()?'logout':'login')?>/"><?=(UserController::isAuth()?'Logout':'Login')?></a>
		</li>
	  </ul>
	</nav>
  </div>
</header>


<div class="container">  
  <div class="page-header"></div>