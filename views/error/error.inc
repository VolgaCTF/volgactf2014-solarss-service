<legend>Error</legend>
<div>
	<div class="alert alert-dismissable alert-danger" role="alert">
	<button type="button" class="close" data-dismiss="alert" onclick='history.back()'>×</button>
	<strong>Attention!</strong><?=$parameters['error']?> </div>
</div>