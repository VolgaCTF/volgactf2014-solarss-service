<?
	session_start();
#	error_log(0);
#	error_reporting(0);
	define("FLAG_TTL", 900);

	include 'functions.php';

	foreach (glob(__dir__.'/controllers/*') as $inc) {
		include $inc;
	}

	$page = isset($_GET['PAGE'])? @array_shift(explode('/', $_GET['PAGE'])) : 'index';

	
	DatabaseController::init();
	AppController::init();

	switch ($page) {
		case 'index':
			MainController::index();
			break;
		
		case 'search':
			MainController::search();
			break;
		
		case 'category':
			MainController::category();
			break;

		case 'showNews':
			MainController::showNews();
			break;

		case 'userPage':
			MainController::userPage();
			break;
		case 'add':
			MainController::add();
			break;

		case 'channels':
			MainController::channels();
			break;

		case 'channel':
			MainController::channel();
			break;

		case 'resetFeeds':
			MainController::resetFeeds();
			break;
				

		case 'login':
			UserController::login();
			break;

		case 'logout':
			UserController::logout();
			break;

		case 'register':
			UserController::register();
			break;


		
		default:
			MainController::index();
			break;
	}
