<?
	class MainController{

		public static function index(){
			$db = DatabaseController::$db_news;

			$articles = array();
			foreach (AppController::$categories as $category) {
				$article = $db->querySingle("SELECT * FROM news WHERE category = '$category' order by addtime DESC limit 1", true);
				$article['link'] = 'showNews/?id='.$article['id'];
				if(strlen($article['title']) > 70)
					$article['title'] = substr($article['title'], 0, 68) . '...';
				$articles[] = $article;
			}

			$user = array();
			$user['category'] = 'User';
			$user['title'] = 'Your Feed';
			$user['description'] = "Your channel";
			$user['link'] = 'userPage/';

			$articles[] = $user;

			render('index.inc', $articles);
		}

		public static function category(){
			if(isset($_GET['category'])){
				$category = $_GET['category'];
				$db = DatabaseController::$db_news;
				$result = $db->query("SELECT * from news WHERE category like \"%" . $db->escapeString($category) . "%\" order by addtime DESC");
				
				$articles = [];
				while ($article = $result->fetchArray()) {
					if($article['category'] == 'Private'){
						if($article['user_id'] != $_SESSION['user_id'])
							continue;
					}	
					$dbu = DatabaseController::$db_users;
					$ures = $dbu->querySingle("SELECT * from users WHERE id = '" . $article['user_id'] . "'", true);
					
					if(empty($ures)){
						$url = parse_url($article['link']);
						$article['author'] = $url['host'];
					}else{
						$article['author'] = $ures['login'];
					}
					
					$articles[] = $article;
				};
				

				render('category.inc', $articles);
			}
			MainController::index();
		}




		public static function showNews(){
			$id = (int) $_GET['id'];
			$db = DatabaseController::$db_news;
			$article = $db->querySingle("SELECT * FROM news WHERE id = '$id'", true);
			
			if($article == FALSE){
				renderError(array('error' => 'No such atricle!'));
			} else {
				if($article['category'] == 'Private'){
					if($article['user_id'] != $_SESSION['user_id'])
						renderError(array('error' => 'No such atricle!'));
				}	
			}
			render('article.inc', array($article));	

		}

		public static function search(){
			$articles = [];
			if(isset($_POST['text'])){
				$db = DatabaseController::$db_news;

				$text = $db->escapeString($_POST['text']);
				$category = @$db->escapeString($_POST['category']);

				$q = "SELECT * from news 
					WHERE (title like \"%$text%\" or description like \"%$text%\")" .
					 (empty($category) ? "and category!='Private'" : " and category like \"%$category%\"") .
					  " order by addtime DESC";
				$result = $db->query($q);
				
				while ($article = $result->fetchArray()) {
					
					$dbu = DatabaseController::$db_users;
					$ures = $dbu->querySingle("SELECT * from users WHERE id = '" . $article['user_id'] . "'", true);
					if(empty($ures)){
						$url = parse_url($article['link']);
						$article['author'] = $url['host'];
					}else{
						$article['author'] = $ures['login'];
					}
					$articles[] = $article;
				};
			}
			render('search.inc', $articles);
		}


		public static function userPage(){
			if(!UserController::isAuth())
				UserController::login();

			if(isset($_POST['action'])){
				switch ($_POST['action']) {
					case 'createChannel':
						
						if(isset($_POST["channelName"]) && isset($_POST["category"])){
							$category = $_POST["category"];
							
							if(in_array($category, array_merge(AppController::$categories,["Private"]))){
								$db = DatabaseController::$db_users;
								$result = $db->query(
								"INSERT INTO channels (user_id, name, category)
								 VALUES (
								'" . $_SESSION['user_id'] . "',
								'" . $db->escapeString($_POST["channelName"]) . "',
								'" . $category . "')");
							}	
						}
						
						break;
					case 'deleteChannel':
						if(isset($_POST["channelId"])){
							$id = (int) $_POST["channelId"];
							
							$db = DatabaseController::$db_users;
							$result = $db->querySingle("SELECT user_id from channels where id='$id'", TRUE);
							if($result['user_id'] == $_SESSION['user_id']){
								$db->exec("DELETE FROM channels where id='$id'");
								$dbn = DatabaseController::$db_news;
								$dbn->exec("DELETE FROM news where channel_id='$id'");
							}	
						}
						break;
					case 'subscribeFeed':
						if(isset($_POST["feedURI"])){
							
							$link = $_POST["feedURI"];
							
							$db = DatabaseController::$db_users;
							$result = $db->query(
							"INSERT INTO feeds (user_id, link)
							 VALUES (
							'" . $_SESSION['user_id'] . "',
							'" . $db->escapeString($_POST["feedURI"]) . "')");
								
						}
						break;
					case 'deleteFeed':
						if(isset($_POST["feedId"])){
							$id = (int) $_POST["feedId"];
							
							$db = DatabaseController::$db_users;
							$result = $db->querySingle("SELECT user_id from feeds where id='$id'", TRUE);
							if(!empty($result) && $result['user_id'] == $_SESSION['user_id']){
								$db->exec("DELETE FROM feeds where id='$id'");
							}	
						}
						break;

					
					default:
						# code...
						break;
				}
			}


			$feeds = [];
			$db = DatabaseController::$db_users;
			$result = $db->query("SELECT * from feeds WHERE user_id = '" . $_SESSION['user_id'] . "'");
			while ($feed = $result->fetchArray()) {
				$rawFeed = file_get_contents_curl($feed['link']);
				$xml = new SimpleXmlElement($rawFeed);

				$c = 0;
				$articles = [];
				foreach ($xml->channel->item as $item) {
					$article = array();
					$article['link'] = $item->link;
					$article['title'] = $item->title;
					$article['pubDate'] = strtotime($item->pubDate);
					$article['description'] = (string) trim($item->description);
		
					$content = $item->children('http://purl.org/rss/1.0/modules/content/');
					$article['content'] = (string)trim($content->encoded);
			 		if(empty($content))
			 			$article['content'] = "Read more <a href='{$article['link']}' target='_blank'>using this link</a>";

			 		$articles[] = $article;
					if($c++ == 4)
						break;
				}
				
		 		$feed['articles'] = $articles;
		 		$feeds[] = $feed;
			}
			
			$channels = [];
			$result = $db->query("SELECT * from channels WHERE user_id = '" . $_SESSION['user_id'] . "'");
			while ($channel = $result->fetchArray()) {
				$dbn = DatabaseController::$db_news;
				$channel['count'] = (int)$dbn->querySingle("SELECT count(*) FROM news where channel_id='{$channel['id']}'");
				$channels[] = $channel;
			}
			render('userPage.inc', array('feeds' => $feeds, 'channels' => $channels));
		}

		public static function add(){
			$channels = [];
			$db = DatabaseController::$db_users;
			$result = $db->query("SELECT * from channels WHERE user_id = '" . $_SESSION['user_id'] . "'");
			
			if(!$result->fetchArray())
				renderError(array('error' => 'To post news you need to create your own channel using personal page'));
			

			if(isset($_POST['title']) && isset($_POST['description']) && isset($_POST['content']) && isset($_POST['channelId'])){
				$id = (int) $_POST["channelId"];
							
				$db = DatabaseController::$db_users;
				$result = $db->querySingle("SELECT * from channels where id='$id'", TRUE);
				if($result['user_id'] != $_SESSION['user_id']){
					renderError(array('error' => 'This channel is not yours!'));
				}
				
				$article = array();
				$article['category'] = $result['category'];
				$article['user_id'] = $result['user_id'];
				$article['channel_id'] = $id;

				$dbn = DatabaseController::$db_news;
				$next = ($dbn->querySingle('SELECT id from news order by id DESC limit 1')) + 1;
				$a = explode('/', $_SERVER['PHP_SELF']);
			  	array_pop($a);
			  	$self =  implode('/', $a);

				$article['link'] = 'http://'.$_SERVER['HTTP_HOST'].$self.'/showNews/?id='.$next;
				$article['title'] = $_POST['title'];
				$article['pubDate'] = time();
				$article['description'] = $_POST['description'];
				$article['addtime'] = time();
				
				// get data held in namespaces
				
				$article['content'] = $_POST['content'];
		 		AppController::insert($article);

				MainController::index();
			}

			$result = $db->query("SELECT * from channels WHERE user_id = '" . $_SESSION['user_id'] . "'");
			
			while ($channel = $result->fetchArray()) {
				$channels[] = $channel;
			}
			render('add.inc', $channels);

		}

		public static function channels(){
			$dbn = DatabaseController::$db_news;
			$dbu = DatabaseController::$db_users;
			
			$channels = [];
			foreach (AppController::$categories as $category) {
				$result = $dbu->query("SELECT * FROM channels where category='$category' order by id DESC limit 10");
				while($channel = $result->fetchArray()) {
					$channel['count'] = (int) $dbn->querySingle("SELECT count(*) FROM news where channel_id='{$channel['id']}'");
					$channels[$category][] = $channel;
				}
			}

			render('channels.inc', $channels);

		}


		public static function channel(){
			if(isset($_GET['id'])){
				$id = (int) $_GET['id'];
				$db = DatabaseController::$db_news;
				$result = $db->query("SELECT * from news WHERE channel_id='$id' order by addtime DESC");
				
				$dbu = DatabaseController::$db_users;
				$author = $dbu->querySingle("SELECT login from users WHERE id = (select user_id from channels where channels.id='$id')");
					
				$name = $dbu->querySingle("SELECT name from channels where id='$id'");


				$articles = [];
				while ($article = $result->fetchArray()) {
					if($article['category'] == 'Private'){
						if($article['user_id'] != $_SESSION['user_id'])
							continue;
					}	
					$articles[] = $article;
				};

				if(isset($_GET['format'])){
					if($_GET['format'] == 'XML'){
						header("Content-type: text/xml"); 

						echo "<?xml version='1.0' encoding='UTF-8'?> 
						<rss version='2.0'>
						<channel>
						<title><![CDATA[$name]]></title>
						<link><![CDATA[http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]]]></link>
						<description>Solar RSS </description>
						<language>en-us</language>"; 

						foreach($articles as $article)
						{
						$title= xml_entities($article['title']); 
						$link= xml_entities($article['link']); 
						$description= xml_entities($article['description']); 
						$content = xml_entities($article['content']);
						$pubDate = xml_entities(date('r', $article['pubDate']) );

						echo "<item> 
						<title>$title</title>
						<link>$link</link>
						<pubDate>$pubDate</pubDate>

						<description>$description</description>
						<content>$content</content>
						</item>"; 
						} 
						echo "</channel></rss>"; 
						die;
					}
				}

				$out['author'] = $author;			
				$out['name'] = $name;
				$out['articles'] = $articles;

				render('channel.inc', $out);
			}
			MainController::index();
		}

		public static function resetFeeds(){
			if(!UserController::isAuth())
				UserController::login();

			if(isset($_POST['sure'])){
				$db = DatabaseController::$db_users;
				$db->exec("DELETE FROM feeds where user_id='{$_SESSION['user_id']}'");
				MainController::userPage();
			}

			render('resetfeeds.inc', null);
		}


	}
