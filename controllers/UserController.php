<?
	class UserController{


		public static function isAuth(){
			return isset($_SESSION['auth']);
		}

		public static function login(){
			if(isset($_POST['login']) && isset($_POST['password'])){
				$login = $_POST['login'];
				$password = $_POST['password'];

				$db = DatabaseController::$db_users;

				$result = $db->querySingle("SELECT id FROM users where \"login\"=\"". $db->escapeString($login) . "\" and \"password\" = \"" . $db->escapeString($password) . "\"", true);

				if(!isset($result['id'])){
					renderError(array('error' => 'user not found'));
				} else {
					$_SESSION['user_id'] = $result['id'];
					$_SESSION['auth'] = TRUE;

					MainController::index();
				}
			}

			render('login.inc', null);
		}

		public static function logout(){
			unset($_SESSION['user_id']);
			unset($_SESSION['auth']);
			MainController::index();

		}

		public static function register(){
			
			if(isset($_POST['login']) && isset($_POST['password'])){
				$login = $_POST['login'];
				$password = $_POST['password'];
				$db = DatabaseController::$db_users;
				$result = $db->querySingle("SELECT id FROM users WHERE login = \"" . $db->escapeString($login) . "\"", true);
				if(!empty($result))
					renderError(array('error' => 'User exists'));

				$result = $db->query("INSERT INTO users (login, password) VALUES ( \"" . $db->escapeString($login) . "\",\"" . $db->escapeString($password) . "\")");
				
				if($result === FALSE)
					renderError($db->lastErrorMsg());
				else
					UserController::login();
			}
			render('register.inc', null);
		}
	}