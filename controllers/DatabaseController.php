<?
	class DatabaseController{
		public static $db_users;
		public static $db_news;

		public static function init(){
			DatabaseController::$db_users = new sqlite3("users.db");
			DatabaseController::$db_users->exec(
				"CREATE TABLE if not exists 
				users (
					id INTEGER PRIMARY KEY autoincrement,
					login CHAR(50) NOT NULL,
					password CHAR(50) NOT NULL
				)");
			DatabaseController::$db_users->exec(
				"CREATE TABLE if not exists 
				channels (
					id INTEGER PRIMARY KEY autoincrement,
					user_id INTEGER NOT NULL,
					name CHAR(100) NOT NULL,
					category CHAR(50) NOT NULL
					
				)");
			
			DatabaseController::$db_users->exec(
				"CREATE TABLE if not exists 
				feeds (
					id INTEGER PRIMARY KEY autoincrement,
					user_id INTEGER NOT NULL,
					link CHAR(255) NOT NULL
				
				)");
			
		


			DatabaseController::$db_news = new sqlite3("news.db");
			DatabaseController::$db_news->exec(
				"CREATE TABLE if not exists 
				news (
					id INTEGER PRIMARY KEY autoincrement,
					user_id INTEGER NOT NULL,
					channel_id CHAR(50) NOT NULL,
					category CHAR(50) NOT NULL,
					link CHAR(255) NOT NULL,
					title TEXT NOT NULL,
					pubDate TEXT NOT NULL,
					description TEXT NOT NULL,
					content TEXT NOT NULL,
					addtime TEXT NOT NULL					
				)");


		}

	}