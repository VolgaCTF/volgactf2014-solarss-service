<? 
	class AppController{

		public static $categories = array(
			"World",
			"Local",
			"Education",
			"Science",
			"Technology",
			"Kids",
			"Sports",
			"Entertainment");
	
		public static function init(){
			
			$db = DatabaseController::$db_news;
			$res = $db->querySingle('SELECT count(*) FROM news;');
			if($res == '0'){
				AppController::update(AppController::$categories);
			}

			$toUpdate = [];
			foreach (AppController::$categories as $category) {
				$res = $db->querySingle('SELECT max(addtime) FROM news where category = "'.$category.'";');
				if( time() - $res > FLAG_TTL)
					$toUpdate[] = $category;
			}

			AppController::update($toUpdate);				
		}	


		public static function update($categories){
			
			$rss = rssList();
			foreach ($categories as $category){
				$r = 0;
				while( $r == 0){
					$link = $rss[$category][mt_rand(0, count($rss[$category]) - 1 )];
					$rawFeed = file_get_contents_curl($link);
					$xml = new SimpleXmlElement($rawFeed);
					$r = count($xml->channel->item);
				}
					
				$item = $xml->channel->item[mt_rand(0, $r - 1 )];
				$article = array();
				$article['category'] = $category;
				$article['user_id'] = '0';
				$article['channel_id'] = '0';
				$article['link'] = $item->link;
				$article['title'] = $item->title;
				$article['pubDate'] = strtotime($item->pubDate);
				$article['description'] = (string) trim($item->description);
				$article['addtime'] = time();
				
				// get data held in namespaces
				
				$content = $item->children('http://purl.org/rss/1.0/modules/content/');
				$article['content'] = (string)trim($content->encoded);
		 		if(empty($content))
		 			$article['content'] = "Read more <a href='{$article['link']}' target='_blank'>using this link</a>";
		 		
				AppController::insert($article);
			}
		}

		public static function insert($article){
			$db = DatabaseController::$db_news;

			$res = $db->query("INSERT INTO news
				(user_id, channel_id,category, link, title, pubDate, description, content, addtime)
				values

				('" . $db->escapeString($article['user_id']) . "',
				 '" . $db->escapeString($article['channel_id']) . "',
				 '" . $db->escapeString($article['category']) . "',
				 '" . $db->escapeString($article['link']) . "',
				 '" . $db->escapeString($article['title']) . "',
				 '" . $db->escapeString($article['pubDate']) . "',
				 '" . $db->escapeString($article['description']) . "',
				 '" . $db->escapeString($article['content']) . "',
				 '" . $db->escapeString($article['addtime']) . "')");

		}

	}

