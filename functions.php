<?

function render($page, $parameters){
	include './views/common/header.inc';
	include './views/app/'.$page;
	include './views/common/footer.inc';
	exit();
}


function renderError($parameters){
	$parameters['title'] = 'Error!';
	include './views/common/header.inc';
	include './views/error/error.inc';
	include './views/common/footer.inc';
	exit();
}

function redirect($page, $parameters){
	$parameters['title'] = 'redirect..';
	include './views/common/header.inc';
	include './views/redirect/redirect.inc';
	include './views/common/footer.inc';
	exit();
}

function rssList(){
	return array(
		"World" => array(
			"http://rss.cbc.ca/lineup/world.xml",
			"http://rss.cnn.com/rss/cnn_topstories.rss",
			"http://rss.nytimes.com/services/xml/rss/nyt/HomePage.xml",
			"http://feeds.washingtonpost.com/rss/homepage",
			"http://hosted.ap.org/lineups/USHEADS-rss_2.0.xml?SITE=RANDOM&SECTION=HOME",
			"http://www.npr.org/rss/rss.php?id=1001",
			"http://feeds.reuters.com/reuters/topNews",
			"http://newsrss.bbc.co.uk/rss/newsonline_world_edition/americas/rss.xml",
			"http://rt.com/rss/",
			"http://blog.getbootstrap.com/feed.xml",
			"http://www.cbc.ca/cmlink/rss-canada",
			"http://rss.cbc.ca/lineup/politics.xml",
			"http://rss.cbc.ca/lineup/canada-britishcolumbia.xml",
			"http://rss.cbc.ca/lineup/canada-calgary.xml",
			"http://rss.cbc.ca/lineup/canada-edmonton.xml",
			"http://rss.cbc.ca/lineup/canada-saskatchewan.xml",
			"http://rss.cbc.ca/lineup/canada-saskatoon.xml",
			"http://rss.cbc.ca/lineup/canada-manitoba.xml",
			"http://rss.cbc.ca/lineup/canada-thunderbay.xml",
			"http://rss.cbc.ca/lineup/canada-sudbury.xml",
			"http://rss.cbc.ca/lineup/canada-windsor.xml",
			"http://rss.cbc.ca/lineup/canada-kitchenerwaterloo.xml",
			"http://rss.cbc.ca/lineup/canada-toronto.xml",
			"http://rss.cbc.ca/lineup/canada-ottawa.xml",
			"http://rss.cbc.ca/lineup/canada-montreal.xml",
			"http://rss.cbc.ca/lineup/canada-newbrunswick.xml",
			"http://rss.cbc.ca/lineup/canada-pei.xml",
			"http://rss.cbc.ca/lineup/canada-novascotia.xml",
			"http://rss.cbc.ca/lineup/canada-newfoundland.xml",
			"http://rss.cbc.ca/lineup/canada-north.xml",
		),
		"Local" => array(
			"http://rss.cbc.ca/lineup/canada.xml",
			"http://www.marsnews.com/xml/marsnews.xml",
			"http://www.liveinternet.ru/users/gamovic/rss",
			"http://www.deseretnews.com/home/index.rss",
			"http://www.ksl.com/xml/148.rss",
			"http://le.utah.gov/rss/utleg.xml",
			"http://www.utah.gov/whatsnew/rss.xml",
			"http://topics.nytimes.com/top/news/national/usstatesterritoriesandpossessions/utah/index.html?inline=nyt-geo&rss=1"
		),
		"Education" => array(
			"http://www.uen.org/feeds/rss/news.xml.php",
			"http://www2.ed.gov/rss/edgov.xml",
			"http://feeds.nytimes.com/nyt/rss/Education",
			"http://www.smartbrief.com/servlet/rss?b=ASCD",
			"http://www.pbs.org/teachers/learning.now/rss2/index.xml",
			"http://www.npr.org/rss/rss.php?id=1013",
			"http://www.techlearning.com/RSS",
			"http://rss.cbc.ca/lineup/business.xml",
		),
		"Science" => array(
			"http://hosted.ap.org/lineups/SCIENCEHEADS-rss_2.0.xml?SITE=OHLIM&SECTION=HOME",
			"http://feeds.sciencedaily.com/sciencedaily",
			"http://www.sciencemag.org/rss/news.xml",
			"http://www.eurekalert.org/rss.xml",
			"http://rss.cbc.ca/lineup/health.xml",

		),
		"Technology" => array(
			"http://www.techlearning.com/RSS",
			"http://feeds.wired.com/wired/index",
			"http://feeds.nytimes.com/nyt/rss/Technology",
			"http://www.npr.org/rss/rss.php?id=1019",
			"http://feeds.feedburner.com/time/gadgetoftheweek",
			"http://feeds.surfnetkids.com/SurfingTheNetWithKids",
			"http://rss.macworld.com/macworld/feeds/main",
			"http://rss.feedsportal.com/c/270/f/3547/index.rss",
			"http://rss.cbc.ca/lineup/technology.xml",
		),
		"Kids" => array(
			"http://www.loc.gov/rss/read/eca.xml",
			"http://dictionary.reference.com/wordoftheday/wotd.rss",
			"http://feeds.feedburner.com/time/photoessays",
			"http://www.sciencenewsforkids.org/feed/",
			"http://rss.cbc.ca/lineup/offbeat.xml",
			"http://www.cbc.ca/cmlink/rss-cbcaboriginal",
		),
		"Sports" => array(
			"http://hosted.ap.org/lineups/SPORTSHEADS-rss_2.0.xml?SITE=VABRM&SECTION=HOME",
			"http://rss.cnn.com/rss/si_topstories.rss",
			"http://feeds1.nytimes.com/nyt/rss/Sports",
			"http://sports.yahoo.com/top/rss.xml",
			"http://rss.cbc.ca/lineup/sports.xml",
			"http://rss.cbc.ca/lineup/sports-mlb.xml",
			"http://rss.cbc.ca/lineup/sports-nba.xml",
			"http://rss.cbc.ca/lineup/sports-curling.xml",
			"http://rss.cbc.ca/lineup/sports-cfl.xml",
			"http://rss.cbc.ca/lineup/sports-nfl.xml",
			"http://rss.cbc.ca/lineup/sports-nhl.xml",
			"http://rss.cbc.ca/lineup/sports-soccer.xml",
			"http://rss.cbc.ca/lineup/sports-figureskating.xml",

		),
		"Entertainment" => array(
			"http://rss.cbc.ca/lineup/arts.xml",
			"http://www.npr.org/rss/rss.php?id=1008",
			"http://newoldage.blogs.nytimes.com/feed/",
			"http://www.npr.org/rss/rss.php?id=13",
			"http://www.npr.org/rss/rss.php?id=1045",
			"http://www.nationalgeographic.com/adventure/nga.xml",
			"http://kino-doma.net/news/rss/",
//			"http://feeds.feedburner.com/topkot?format=xml",
			"http://rss.cbc.ca/lineup/topstories.xml",
			"http://feeds.feedburner.com/geekcity/geekfeed?format=xml",
		)
	);
}

function xml_entities($string) {
	return strtr(
		$string, 
		array(
			"<" => "&lt;",
			">" => "&gt;",
			'"' => "&quot;",
			"'" => "&apos;",
			"&" => "&amp;",
		)
	);
}


function file_get_contents_curl($url){
	$ch = curl_init();

	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	@curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
	curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
	curl_setopt($ch, CURLOPT_HEADER, true);

	$data = curl_exec($ch);

	$info = curl_getinfo($ch);
	$rbody = $info["size_download"] ? substr($data, $info["header_size"], $info["size_download"]) : "";

	$rheaders = substr($data, 0, $info["header_size"]);
	curl_close ($ch);

	return $rbody;
}
